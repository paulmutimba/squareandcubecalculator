//Paul Mutimba
//Square and Cube Functions

#include<conio.h>
#include<iostream>

using namespace std;

float Square(float a)
{
	return (a * a);
}

float Cube(float b)
{
	return (b * b * b);
}

int main()
{
	float num;
	int calc;
	string lp;

	do {

		cout << "Enter a Number: \n";
		cin >> num;

		do
		{
			cout << "Enter 2 for Square Or 3 for Cube: \n";
			cin >> calc;

		} while (calc != 3 && calc != 2);
		

		switch (calc)
		{
		case 2:
			cout << "The Square is: " << Square(num) << endl;
			break;

		case 3:
			cout << "The Cube is: " << Cube(num) << endl;
			break;

		default:
			cout << "Enter a 2 or 3"<< "\n"; //if input is not a 2 or 3
			break;
		}

		cout << "Perform another Calculation? (y/n) \n";
		cin >> lp;
		
	} while (lp == "y");

	(void)_getch();
	return 0;
}